package com.example.booknerd.view.book

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.booknerd.databinding.ItemBookBinding
import com.example.booknerd.model.local.entity.Book

class BookAdapter : RecyclerView.Adapter<BookAdapter.BookViewHolder>() {

    private var book = mutableListOf<Book>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BookViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val bookData = book[position]
        holder.bindBook(bookData)
    }

    override fun getItemCount(): Int {
        return book.size
    }

    fun loadBook(item: List<Book>) {
        this.book = item.toMutableList()
        notifyDataSetChanged()
    }

    class BookViewHolder(
        private val binding: ItemBookBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindBook(book : Book) {
            binding.tvTitle.text = book.title
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemBookBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { BookViewHolder(it) }
        }
    }
}