package com.example.booknerd.view.book

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.booknerd.databinding.FragmentBookBinding
import com.example.booknerd.viewmodel.BookViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookFragment : Fragment() {

    private var _binding: FragmentBookBinding? = null
    private val binding get() = _binding!!
    private val bookViewModel by viewModels<BookViewModel>()
    private val bookAdapter by lazy { BookAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentBookBinding.inflate(inflater, container, false).also { _binding = it}.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvBook.adapter = bookAdapter

        bookViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.rvBook.layoutManager = LinearLayoutManager(context)
            bookAdapter.loadBook(state.book)
//            binding.rvBook.adapter = bookAdapter.apply { loadBook(state.book) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}