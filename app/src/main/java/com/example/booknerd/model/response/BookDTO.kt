package com.example.booknerd.model.response


import com.google.gson.annotations.SerializedName

data class BookDTO(

//    val author: List<String>,
    val id: String,
    val title: String,
//    @SerializedName("wiki_url")
//    val wikiUrl: String,
//    val year: String
)
