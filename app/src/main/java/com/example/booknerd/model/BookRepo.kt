package com.example.booknerd.model

import android.content.Context
import com.example.booknerd.model.local.BookDatabase
import com.example.booknerd.model.local.dao.BookDao
import com.example.booknerd.model.local.entity.Book
import com.example.booknerd.model.remote.BookService
import com.example.booknerd.model.response.BookDTO
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BookRepo @Inject constructor(
    private val service: BookService,
    private val bookDao: BookDao
) {

//    private val bookService = BookService
//    private val bookDatabase = BookDatabase.getInstance(context).bookDao()

    suspend fun getBooks() = withContext(Dispatchers.IO) {
        val cachedBooks: List<Book> = bookDao.getAll()

        return@withContext cachedBooks.ifEmpty {
            val bookData = service.getBooks()
            val books: List<Book> = bookData.map {
                Book(title = it.title)
            }
//            val books: List<Book> = service.getBooks().map { Book(title = it.title)}
//            bookDatabase.insert(books)
            bookDao.insert(books)
            return@ifEmpty books
        }
    }
}