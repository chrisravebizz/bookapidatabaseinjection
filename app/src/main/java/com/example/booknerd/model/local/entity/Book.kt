package com.example.booknerd.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.booknerd.model.response.BookDTO

@Entity
data class Book(

    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val title: String
)