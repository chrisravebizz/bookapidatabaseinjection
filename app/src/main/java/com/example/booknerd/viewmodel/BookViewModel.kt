package com.example.booknerd.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.example.booknerd.model.BookRepo
import com.example.booknerd.model.local.entity.Book
import com.example.booknerd.model.response.BookDTO
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(
    private val repo: BookRepo
) : ViewModel() {

//    private val repo by lazy { BookRepo }
    private val _state = MutableLiveData(BookState(isLoading = true))
    val state: LiveData<BookState> get() = _state

    init {
        viewModelScope.launch {
            val bookDTO = repo.getBooks()
            _state.value = BookState(book = bookDTO)
        }
    }

//    val state: LiveData<BookState> = liveData {
//        emit(BookState(isLoading = true))
//        val books = repo.getBooks()
//        emit(BookState(book = books))
//    }

    class BookState(
        val isLoading: Boolean = false,
        val book: List<Book> = emptyList()
    )

}