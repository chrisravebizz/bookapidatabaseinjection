package com.example.booknerd.di

import android.content.Context
import com.example.booknerd.model.local.BookDatabase
import com.example.booknerd.model.local.dao.BookDao
import com.example.booknerd.model.remote.BookService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val BASE_URL = "https://the-dune-api.herokuapp.com"

    @Provides
    @Singleton
    fun providesInstance() : BookService {
        val retrofit : Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create()
    }

    @Provides
    @Singleton
    fun providesBookDao(@ApplicationContext context: Context) : BookDao = BookDatabase.getInstance(context).bookDao()

}